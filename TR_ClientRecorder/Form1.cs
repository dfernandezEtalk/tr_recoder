﻿using Etalk.Utilities;
using Newtonsoft.Json;
using RecorderTool;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TR_ClientRecorder
{
    public partial class Form1 : Form
    {
        private bool isRecording;
        private bool isLogged;
        private Recorder rec;
        private ConfigManager config;
        private UserInfo userInfo;
        private Tags tags;
        private Logger myLog;
        private string stopTime;
        private List<string> tagsSelected;

        private System.Windows.Forms.ContextMenu recorderMenu;
        private System.Windows.Forms.MenuItem logInOut;
        private System.Windows.Forms.MenuItem tagsOptions;
        private System.Windows.Forms.MenuItem exit;


        public Form1()
        {
            InitializeComponent();
            tagsSelected = new List<string>();

            myLog = Logger.GetInstance("TR_MicroEngine");
            myLog.open();
            myLog.log("TR_MicroEngine V: " + Assembly.GetExecutingAssembly().GetName().Version.ToString(), false, true);

            rec = new Recorder();
            config = ConfigManager.GetConfig(ConfigType.XML, "Config.xml");
            rec.Device = config.GetAttributeValue("config/microphone", "name");
            notifyIcon.BalloonTipTitle = "TR_MicroEngine";
            
            this.recorderMenu = new System.Windows.Forms.ContextMenu();
            this.logInOut = new System.Windows.Forms.MenuItem();
            this.tagsOptions = new System.Windows.Forms.MenuItem();
            this.exit = new System.Windows.Forms.MenuItem();

            this.recorderMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.logInOut, this.tagsOptions, this.exit});

            this.logInOut.Index = 0;
            this.logInOut.Text = "Login";
            this.logInOut.Click += new System.EventHandler(this.LogOut);

            this.tagsOptions.Index = 1;
            this.tagsOptions.Text = "Clasificación";
            this.tagsOptions.Enabled = false;

            this.exit.Index = 2;
            this.exit.Text = "Salir";
            this.exit.Click += new System.EventHandler(this.CloseApp);

            notifyIcon.ContextMenu = this.recorderMenu;
            SetControls(false);
            notifyIcon.Icon = new Icon(@"icons\home.ico");
            notifyIcon.Text = "Iniciar Sesión";
            isLogged = false;

        }

        private void SetControls(bool record)
        {
            if (!record)
            {
                notifyIcon.Icon = new Icon(@"icons\rec.ico");
                notifyIcon.Text = "Grabar";
            }
            else
            {
                notifyIcon.Icon = new Icon(@"icons\stop.ico");
                notifyIcon.Text = "Detener";
            }
            isRecording = record;
            logInOut.Enabled = !isRecording;
            tagsOptions.Enabled = isRecording;
        }

        private void ExecuteTaks(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;
            if (!isLogged) return;
            if (!isRecording)//Start record
            {
                myLog.log("Recording...");
                Notify("Grabando");
                rec.StartRecording();
                SetControls(true);
            }
            else//Stop record
            {
                myLog.log("Stopping..");
                Notify("Deteniendo");
                rec.StopRecording();
                SetControls(false);
                stopTime = $"{ DateTime.Now:yyyy-MM-dd_HH:mm:ss}";
                myLog.log($"{stopTime}");
                int port = int.Parse(config.GetAttributeValue("config/trCollector", "port"));
                string ip = config.GetAttributeValue("config/trCollector", "ip");

                string userPath = $"{userInfo.Name}_{userInfo.FirstName}_{userInfo.LastName}_{userInfo.ExtRef}";
                string targetPath = Path.Combine(userPath, rec.OutputPath);
                string sharedFolder = config.GetAttributeValue("config/sharedFolder", "path");
                targetPath = Path.Combine(sharedFolder, targetPath);

                string message = $"SaveCallInfo|File>{rec.File}|Ext>{userInfo.ExtRef}|Protocol>ON_CLIENT|AgentId>-1|From>[IP_MACHINE]|To>{userInfo.FullName}|CallID>{Guid.NewGuid().ToString("N")}|TriggerStopTime>{stopTime}|State>COMPLETE|MicroEngine>{Path.Combine(targetPath, rec.OutputFile)}|UserId>{userInfo.Id}|Classifications>[{string.Join("|", tagsSelected)}]^";
                tagsSelected.Clear();
                myLog.log($"Ip:{ip}, Port:{port},Send: {message}");
                Tuple<int, string> result = SocketClient.SendMessage(message, ip, port);
                if (result.Item1 == 0)
                {
                    Notify("Grabacion guardada");
                    myLog.log("Saved");
                    Directory.CreateDirectory(targetPath);
                    Task task = new Task(() => MoveWavFile(rec.File, targetPath));
                    task.Start();
                }
                else
                {
                    Notify("Algo salio mal");
                }
            }
        }

        private void MoveWavFile(string sourcePath, string targetPath)
        {
            string fileName = Path.GetFileName(sourcePath);
            File.Copy(sourcePath, Path.Combine(targetPath, fileName));
            Notify("Se guardo en el Repositorio");
        }

        private void Notify(string message)
        {
            notifyIcon.BalloonTipText = message;
            notifyIcon.ShowBalloonTip(5);
        }

        private void LogOut(object Sender, EventArgs e)
        {
            Show();
            notifyIcon.Icon = new Icon(@"icons\home.ico");
            txtPassword.Text = "";
            txtUserName.Text = "";
            isLogged = false;
            this.logInOut.Text = "Login";
            this.tagsOptions.MenuItems.Clear();
        }

        private void CloseApp(object Sender, EventArgs e)
        {
            recorderMenu.Dispose();
            this.Close();
        }

        private void SetTag(object Sender, EventArgs e)
        {
            string tagOption = ((MenuItem)Sender).Text;
            int value = tags.ListSource.Find(tag => tag.Text == tagOption).Value;
            if (!tagsSelected.Contains(value.ToString()))
                tagsSelected.Add(value.ToString());
        }

        private void LogIn(object Sender, EventArgs e)
        {
            Show();
        }

        private void LoginKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && txtUserName.Text.Length > 0 && txtPassword.Text.Length > 0)
            {
                LoginUser();
            }
        }

        private void LoginUser()
        {
            BtnLogin.Enabled = false;
            try
            {
                string loginUrl = config.GetAttributeValue("config/api", "url") + config.GetAttributeValue("config/api/login", "route");
                HttpWebRequest loginRequest = (HttpWebRequest)WebRequest.Create(loginUrl);
                loginRequest.ContentType = "application/json";
                loginRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(loginRequest.GetRequestStream()))
                {
                    string userName = $"\"UserName\":\"{txtUserName.Text}\",";
                    string password = $"\"Password\":\"{txtPassword.Text}\",";
                    string json = string.Format("{0}{1}\"RememberUser\":\"false\"", userName, password);
                    streamWriter.Write("{" + json + "}");
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var loginResponse = (HttpWebResponse)loginRequest.GetResponse();
                using (var loginInfo = new StreamReader(loginResponse.GetResponseStream()))
                {
                    string loginJson = loginInfo.ReadToEnd();
                    Result loginResult = JsonConvert.DeserializeObject<Result>(loginJson);
                    myLog.log($"UserName:{txtUserName.Text}");
                    if (loginResult.ReturnCode == 0)
                    {
                        myLog.log("Login Correct");
                        string infoUrl = config.GetAttributeValue("config/api", "url") + config.GetAttributeValue("config/api/info", "route");

                        HttpWebRequest infoRequest = (HttpWebRequest)WebRequest.Create($"{infoUrl}?userName={txtUserName.Text}");
                        infoRequest.CookieContainer = new CookieContainer();

                        Regex cookies = new Regex(@"ASP.NET_SessionId=(?<SessionId>.+);.path=(?<path>.+);.+signInForm=(?<form>.+);.+expires=(?<expires>.*);.path=(?<path2>.+);.+");
                        Match cookiesResult = cookies.Match(loginResponse.Headers["Set-Cookie"].ToString());
                        if (cookiesResult.Success)
                        {
                            Uri target = new Uri(config.GetAttributeValue("config/api", "url"));
                            Cookie signInForm = new Cookie("signInForm", cookiesResult.Groups["form"].ToString()) { Domain = target.Host, };
                            infoRequest.CookieContainer.Add(signInForm);
                            HttpWebResponse response = (HttpWebResponse)infoRequest.GetResponse();

                            StreamReader stream = new StreamReader(response.GetResponseStream());
                            string infoJson = stream.ReadToEnd();
                            myLog.log($"Response:{infoJson}");
                            userInfo = JsonConvert.DeserializeObject<UserInfo>(infoJson);
                            stream.Dispose();
                            if (userInfo != null)
                            {
                                string tagsUrl = config.GetAttributeValue("config/api", "url") + config.GetAttributeValue("config/api/tags", "route");
                                infoRequest = (HttpWebRequest)WebRequest.Create(tagsUrl);
                                infoRequest.CookieContainer = new CookieContainer();
                                infoRequest.CookieContainer.Add(signInForm);
                                response = (HttpWebResponse)infoRequest.GetResponse();
                                
                                stream = new StreamReader(response.GetResponseStream());
                                infoJson = stream.ReadToEnd();
                                tags = JsonConvert.DeserializeObject<Tags>(infoJson);
                                stream.Dispose();

                                foreach (var tag in tags.ListSource)
                                {
                                    MenuItem option = new MenuItem();
                                    option.Index = tag.Value;
                                    option.Text = tag.Text;
                                    option.Click += new System.EventHandler(this.SetTag);
                                    this.tagsOptions.MenuItems.Add(option);
                                }

                                isLogged = true;
                                CreateNotifyIcon();
                            }
                            else
                            {
                                myLog.log("UserInfo null");
                                MessageBox.Show("Error con el servidor, iniciar sesion de nuevo");
                            }
                        }
                        else
                        {
                            myLog.log($"cookie wrong={loginResponse.Headers["Set-Cookie"].ToString()}");
                        }
                    }
                    else
                    {
                        myLog.log("Login wrong");
                        MessageBox.Show(loginResult.ReturnMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.log($"{ex.ToString()}", true);
            }
            BtnLogin.Enabled = true;
        }

        private void CreateNotifyIcon()
        {
            this.logInOut.Text = "Logout";
            Hide();
            SetControls(false);
        }

        private void LoginClick(object sender, EventArgs e)
        {
            LoginUser();
        }

        //private void BeforeClose(object sender, EventArgs e)
        //{
        //    ((System.ComponentModel.CancelEventArgs)e).Cancel = true;
        //    Hide();
        //    //if (MessageBox.Show("¿Estás a punto de cerrar MicroEngine. ¿Quieres continuar?", "Salir y cerrar", MessageBoxButtons.YesNo) == DialogResult.No)
        //    //{
        //    //    ((System.ComponentModel.CancelEventArgs)e).Cancel = true;
        //    //}

        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }

    class Result
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
    }

    class UserInfo
    {
        public int GroupId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string TelNumber { get; set; }
        public int StatusId { get; set; }
        public bool LockedOut { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }
        public bool Active { get; set; }
        public object Description { get; set; }
        public string ExtRef { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }

    class Tags
    {
        public List<Tag> ListSource { get; set; }
        public List<string> ArrayValues { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public string ExtRef { get; set; }
        public bool Active { get; set; }
    }

    class Tag
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}
