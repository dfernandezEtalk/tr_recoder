﻿using Etalk.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TR_ClientRecorder
{
    class SocketClient
    {
        public static Tuple<int,string> SendMessage(string message, string ip, int port)
        {
            Logger myLog = Logger.GetInstance("TR_MicroEngine");
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];
            // Connect to a remote device.  
            IPAddress ipAddress = null;
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                
                foreach (IPAddress item in ipHostInfo.AddressList)
                {
                    if (item.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = item;
                        message = message.Replace("[IP_MACHINE]", ipAddress.ToString());
                        break;
                    }
                }

                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(ip), 11000);

                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);

                    // Encode the data string into a byte array.  
                    byte[] msg = Encoding.ASCII.GetBytes($"SktTrMicEng|{message}");

                    // Send the data through the socket.  
                    int bytesSent = sender.Send(msg);

                    // Release the socket.  
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    myLog.log($"ArgumentNullException : {ane.ToString()}", true);
                    return new Tuple<int, string>(1, ipAddress.ToString());
                }
                catch (SocketException se)
                {
                    myLog.log($"ArgumentNullException : {se.ToString()}", true);
                    return new Tuple<int, string>(2, ipAddress.ToString());
                }
                catch (Exception e)
                {
                    myLog.log($"ArgumentNullException : {e.ToString()}", true);
                    return new Tuple<int, string>(3, ipAddress.ToString());
                }

            }
            catch (Exception e)
            {
                myLog.log($"{e.ToString()}", true);
                return new Tuple<int, string>(100, ipAddress.ToString());
            }
            return new Tuple<int, string>(0, ipAddress.ToString());
        }
    }
}
