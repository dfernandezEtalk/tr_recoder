﻿using Etalk.Utilities;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecorderTool
{
    public class Recorder
    {
        private IWaveIn captureDevice;
        private WaveFileWriter writer;
        private Logger myLog;
        private string outputFilename;
        private readonly string outputFolder;
        private string _device = string.Empty;
        private int _deviceIndex = 0;
        private int _sampleRatesIndex = 0;
        private int _channelsIndex = 0;
        private string _file = string.Empty;
        private string _outputPath = string.Empty;
        private string _outputFile = string.Empty;

        public WaveInCapabilities[] Devices
        {
            get
            {
                var devices = Enumerable.Range(-1, WaveIn.DeviceCount + 1).Select(n => WaveIn.GetCapabilities(n)).ToArray();
                //foreach (var device in devices)
                //{
                //    myLog.log($"{device.ProductName}");
                //}
                return devices;
            }
        }

        public int DeviceIndex
        {
            set
            {
                _deviceIndex = value;
            }
            get
            {
                if (string.IsNullOrEmpty(_device))
                    return _deviceIndex;
                else
                {
                    var index = 0;
                    foreach (var device in this.Devices)
                    {
                        if (device.ProductName == _device)
                        {
                            _deviceIndex = index;
                            break;
                        }
                        index++;
                    }
                    return _deviceIndex;
                }
            }
        }

        public string Device
        {
            set
            {
                _device = value;
            }
            get
            {
                return _device;
            }
        }

        public Int32[] SampleRates
        {
            get
            {
                return new Int32[] { 8000, 16000, 22050, 32000, 44100, 48000 };
            }
        }

        public int SampleRatesIndex
        {
            set
            {
                _sampleRatesIndex = value;
            }
        }

        public object Channels
        {
            get
            {
                return new[] { "Mono", "Stereo" };
            }
        }

        public int ChannelsIndex
        {
            set
            {
                _channelsIndex = value;
            }
        }

        public string File
        {
            get
            {
                return _file;
            }
        }

        public string OutputPath
        {
            get
            {
                return _outputPath;
            }
        }

        public string OutputFile
        {
            get
            {
                return _outputFile;
            }
        }

        public Recorder()
        {
            _outputPath = $"{DateTime.Now:yyyy_MM_dd}";
            string savePath = $"TR_MicroEngine\\{_outputPath}";
            outputFolder = Path.Combine(Path.GetTempPath(), savePath);
            Directory.CreateDirectory(outputFolder);
            //if (useLog)
            //{
            //    myLog = Logger.GetInstance("TR_MicroEngine");
            //    myLog.log($"Path: {savePath}");
            //}
        }

        public void StartRecording()
        {
            Cleanup();

            if (captureDevice == null)
            {
                CreateWaveInDevice();
            }
            //MMDeviceEnumerator deviceEnum = new MMDeviceEnumerator();
            //var devices = deviceEnum.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).ToList();
            //var device = (MMDevice)devices[0];
            //device.AudioEndpointVolume.Mute = false;

            outputFilename = GetFileName();
            _file = Path.Combine(outputFolder, outputFilename);
            writer = new WaveFileWriter(Path.Combine(outputFolder, outputFilename), captureDevice.WaveFormat);
            //myLog.log($"Device:{device.ToString()}, Output:{outputFilename}");
            captureDevice.StartRecording();
        }

        public void StopRecording()
        {
            captureDevice?.StopRecording();
        }

        private string GetFileName()
        {
            _outputFile = $"{DateTime.Now:HH-mm-ss_}.wav";
            return _outputFile;
        }

        private void Cleanup()
        {
            if (captureDevice != null)
            {
                captureDevice.Dispose();
                captureDevice = null;
            }
            FinalizeWaveFile();
        }

        private void FinalizeWaveFile()
        {
            writer?.Dispose();
            writer = null;
        }

        private void CreateWaveInDevice()
        {
            var deviceNumber = this.DeviceIndex -1;
            captureDevice = new WaveIn() { DeviceNumber = deviceNumber };
            Int32 sampleRate = SampleRates[_sampleRatesIndex];
            Int32 channels = _channelsIndex + 1;
            captureDevice.WaveFormat = new WaveFormat(sampleRate, channels);
            captureDevice.DataAvailable += OnDataAvailable;
            captureDevice.RecordingStopped += OnRecordingStopped;
        }

        private void OnDataAvailable(object sender, WaveInEventArgs e)
        {
            writer.Write(e.Buffer, 0, e.BytesRecorded);
            int secondsRecorded = (int)(writer.Length / writer.WaveFormat.AverageBytesPerSecond);
        }

        private void OnRecordingStopped(object sender, StoppedEventArgs e)
        {
            FinalizeWaveFile();
        }
    }
}
