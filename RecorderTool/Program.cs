﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecorderTool
{
    class Program
    {
        static void Main(string[] args)
        {
            Recorder recorder = new Recorder();
            if (args.Length == 1)
            {
                switch (args[0])
                {
                    case "Devices":
                        foreach(var item in recorder.Devices)
                            Console.WriteLine(item.ProductName);
                        break;
                    default:
                        Console.WriteLine("Invalid command");
                        break;
                }
            }
        }
    }
}
